/**
 * Panda PacMan
 * 
 */

package Model;

/**
 * the tables are used to speed up computation
 */
public class Resources// should be in model
{
	// for direction computation
	public static final int[] iXDirection={1,0,-1,0};
	public static final int[] iYDirection={0,-1,0,1};
	public static final int[] iDirection=
	{
		-1,	// 0:
		1,	// 1: x=0, y=-1
		-1,	// 2:
		-1,	// 3:
		2,	// 4: x=-1, y=0
		-1,	// 5:
		0,	// 6: x=1, y=0
		-1,	// 7
		-1,     // 8
		3     	// 9: x=0, y=1
	};

	// backward direction
	public static final int[] iBack={2,3,0,1};

	// direction code
	public static final int RIGHT=0;
	public static final int UP=1;
	public static final int LEFT=2;
	public static final int DOWN=3;

	// the maze difinition string
	public static final String[] MazeDefine=
	{
		"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",	// 1
		"X.......XXXX....................XXXX.....X",	// 2
		"X......X....X..................X....X....X",	// 3
		"X......X..O..X................X.....X....X",	// 4
		"X......X......XXXXXXXXXXXXXXXX......X....X",	// 5
		"X.......X..........................X.....X",	// 6
		"X.......----X..................XXXX......X",	// 7
		"X.......-...X..O..........O.....X........X",	// 8
		"X.......-...X.....X    X........X........X",	// 9
		"X.......XXXXX.....X    X........X........X",	// 10
		"X...........X.....XXXXXX........X........X",	// 11
		"X...........X..X..........X.....X........X",	// 12
		"X...........X...X........X......X........X",	// 13
		"X...........X....XXXXXXXX......X.........X",	// 14
		"X............X................X..........X",	// 15
		"X.............XXXXXXXXXXXXXXXX...........X",	// 16
		"X........................................X",	// 17 for expanding the maze! plus check in Maze class HEIGHT=16 and WIDTH=21 if we want bigger maze change those first!
		"X....O...................................X",	// 18
		"X...........................P............X",	// 19
		"X........................................X",	// 20
		"X........................................X",	// 21
		"X............Q................O..........X",	// 22
		"X........................................X",	// 23
		"X........................................X",	// 24
		"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",	// 25
		
	//	 123456789012345678901234567890123456789012			colums number
	//	 000000000111111111122222222223333333333444    		
	};


}

