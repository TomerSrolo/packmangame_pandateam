/**
 * Panda PacMan
 * 
 */

package Model;

import java.awt.*;

import View.Images;

public class SilverCandy implements Candy
{
	final int iX[]={10,15,27,5,30};
	final int iY[]={3,7,7,17,21};

	final int iShowCount=32;
	final int iHideCount=16;

	int frameCount;
	int showStatus;

	int iValid[];

	// the applet this object is associated to
	Window applet;
	Graphics graphics;

	// the ghosts it controls
	Ghost [] ghosts;

	// the silver candy image
	Image imageSilverCandy;

	// the blank image
	public Image imageBlank;

	public SilverCandy(Window a, Graphics g)
	{
		applet=a;
		graphics=g;
		

		// initialize silver candy and image
		iValid = new int[5];

		imageSilverCandy=applet.createImage(16,16);
		Images.drawSilverCandy(imageSilverCandy);

		imageBlank=applet.createImage(16,16);
		Graphics imageG=imageBlank.getGraphics();
		imageG.setColor(Color.black);
		imageG.fillRect(0,0,16,16);

		frameCount=iShowCount;
		showStatus=1;	// show
	}

	public void start()
	{
		// silver candy available
		for (int i=0; i<5; i++)
			iValid[i]=1;
	}

	public void clear(int dot)
	{
		//remove candy from game
		graphics.drawImage(imageBlank, iX[dot]*16, iY[dot]*16, applet);
	}

	public void eat(int iCol, int iRow)
	{
		for (int i=0; i<5; i++)
		{
			if (iX[i]==iCol && iY[i]==iRow)
			{
				iValid[i]=0;
				clear(i);
			}
		}
	}

	public void draw()
	{
		frameCount--;
		if (frameCount==0)
		{
			if (showStatus==1)
			{
				showStatus=0;
				frameCount=iHideCount;
			}
			else
			{
				showStatus=1;
				frameCount=iShowCount;
			}
		}
		for (int i=0; i<5; i++)
		{
			if (iValid[i]==1 && showStatus==1)
				graphics.drawImage(imageSilverCandy,iX[i]*16, iY[i]*16, applet);
			else
				graphics.drawImage(imageBlank, iX[i]*16, iY[i]*16, applet);
		}
	} 
}
