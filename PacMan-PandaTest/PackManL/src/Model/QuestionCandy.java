package Model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;

import javax.swing.JFrame;

import View.Images;
import View.MainFrame;

public class QuestionCandy implements Candy {
	
		final int iX=13;
		final int iY=21;

		final int iShowCount=32;
		final int iHideCount=16;

		int frameCount;
		int showStatus;

		int iValid;
		
		boolean startGhosts=false;
		
		
		

		// the applet this object is associated to
		Window applet;
		Graphics graphics;

		// the question candy image
		Image imageQuestionCandy;

		// the blank image
		public Image imageBlank;
		

		public QuestionCandy(Window a, Graphics g)
		{
			applet=a;
			graphics=g;
			

			// initialize question candy	and image
			iValid = 1;

			imageQuestionCandy=applet.createImage(16,16);
			Images.drawQuestionCandy(imageQuestionCandy);
			imageBlank=applet.createImage(16,16);
			Graphics imageG=imageBlank.getGraphics();
			imageG.setColor(Color.black);
			imageG.fillRect(0,0,16,16);

			frameCount=iShowCount;
			showStatus=1;	// show
		}

		public void start()
		{
			// poison candy available	
			iValid=1;
		}

		public void clear(int dot)
		{
			//remove candy from game
			graphics.drawImage(imageBlank, iX*16, iY*16, applet);
			setStartGhosts(false);
		}

		public void eat(int iCol, int iRow)
		{
				if (iX==iCol && iY==iRow)
				{
					
					iValid=0;
					clear(1);
					
					
					
					
					
					
				}
		}
		
		public boolean isStartGhosts() {
			return startGhosts;
		}

		public void setStartGhosts(boolean startGhosts) {
			this.startGhosts = startGhosts;
		}

		public void draw()
		{
			frameCount--;
			if (frameCount==0)
			{
				if (showStatus==1)
				{
					showStatus=0;
					frameCount=iHideCount;
				}
				else
				{
					showStatus=1;
					frameCount=iShowCount;
				}
			}
			if (iValid==1 && showStatus==1)
				graphics.drawImage(imageQuestionCandy,iX*16, iY*16, applet);
			else
					graphics.drawImage(imageBlank, iX*16, iY*16, applet);
		} 
		
		public void chooseQuestion() {
			
		}

}
