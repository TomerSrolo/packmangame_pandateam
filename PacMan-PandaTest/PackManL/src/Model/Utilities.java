/**
 * Panda PacMan
 * 
 */

package Model;


/**
 * provide some global public utility functions for random outputs
 */
public class Utilities// should be in model
{
	public static int randDo(int iOdds)
		// see if it happens within a probability of 1/odds
	{
		if ( Math.random()*iOdds < 1 )
			return(1);
		return(0);
	}	

	// return a random number within [0..iTotal)
	public static int randSelect(int iTotal)
	{
		double a;
		a=Math.random();
		a=a*iTotal;
		return( (int) a );
	}

	public static int intSign(int iD)
	{
		if (iD==0)
			return(0);
		if (iD>0)
			return(1);
		else
			return(-1);
	}
}
