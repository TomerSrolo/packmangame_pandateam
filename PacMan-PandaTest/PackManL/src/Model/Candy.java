/**
 * Panda PacMan
 * 
 */
package Model;


public interface Candy {
	
	public void start();
	

	public void clear(int dot);
	
	
	public void eat(int iCol, int iRow);
	

	public void draw();
	

}
