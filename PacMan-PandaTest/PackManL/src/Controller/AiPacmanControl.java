/**
 * Panda PacMan
 * 
 */

package Controller;

import Model.Ghost;
import Model.Maze;
import Model.Pacman;
import Model.Resources;

/**
 * cpacmove.java
 * calculate the move for pac man
 * this is used for machine controlled pacman
 * for demonstration or else...
 */
class AiPacmanControl // should be in controller
{
	// things that affect the decision:
	// ghosts status
	//   ghosts position
	//   ghosts movement direction
	// dots position
	//   ONLY the closest dot is goal
	// powerdot position
	//   the closer the ghosts, more weight to go to powerdot

	// direction score
	// each represents the score for that direction
	// the direction with the highest score will be chosen
	// if the chosen one is not available, the oposite score will be subtracted,
	//   and the rest compared again.
	int iDirScore[];

	int iValid[]; 

	Pacman pacman;
	Ghost [] ghosts;
	Maze maze;

	AiPacmanControl(Pacman pac, Ghost ghost[], Maze maze)
	{
		iDirScore=new int[4];

		iValid=new int [4];
		pacman=pac;

		ghosts=new Ghost[4];
		for (int i=0; i<4; i++)
			ghosts[i]=ghost[i];

		this.maze=maze;
	}

	public int getNextDir()
	{
		int i;

		// first, init to 0
		for (i=0; i<4; i++)
			iDirScore[i]=0;

		// add score for dot
		addDotScore();

		// add score for ghosts
		addGhostScore();

		// add score for powerdot
		addPowerDotScore();

		// determine the direction based on scores

		for (i=0; i<4; i++)
			iValid[i]=1;

		int iHigh, iHDir;

		while (true) 
		{
			iHigh=-1000000;
			iHDir=-1;
			for (i=0; i<4; i++)
			{
				if (iValid[i] == 1 && iDirScore[i]>iHigh)
				{
					iHDir=i;
					iHigh=iDirScore[i];
				}
			}

			if (iHDir == -1)
			{
				throw new Error("cpacmove: can't find a way?");
			}

			if ( pacman.iX%16 == 0 && pacman.iY%16==0)
			{
				if ( pacman.mazeOK(pacman.iX/16 + Resources.iXDirection[iHDir],
						pacman.iY/16 + Resources.iYDirection[iHDir]) )
					return(iHDir);
			}
			else
				return(iHDir);

			iValid[iHDir]=0;
			//			iDirScore[ctables.iBack[iHDir]] = iDirScore[iHDir];

		}

		//	return(iHDir);  // will not reach here, ordered by javac
	}

	void addGhostScore()
	{
		int iXDis, iYDis;	// distance
		double iDis;		// distance

		int iXFact, iYFact;

		// calculate ghosts one by one
		for (int i=0; i<4; i++)
		{
			iXDis=ghosts[i].iX - pacman.iX;
			iYDis=ghosts[i].iY - pacman.iY;

			iDis=Math.sqrt(iXDis*iXDis+iYDis*iYDis);

			if (ghosts[i].iStatus == Ghost.BLIND)
			{
				// TODO

			}
			else
			{
				// adjust iDis into decision factor

				iDis=18*13/iDis/iDis;
				iXFact=(int)(iDis*iXDis);
				iYFact=(int)(iDis*iYDis);

				if (iXDis >= 0)
				{
					iDirScore[Resources.LEFT] += iXFact;
				}
				else
				{
					iDirScore[Resources.RIGHT] += -iXFact;
				}

				if (iYDis >= 0)
				{
					iDirScore[Resources.UP] += iYFact;
				}
				else
				{
					iDirScore[Resources.DOWN] += -iYFact;
				}
			}
		}
	}

	void addDotScore()
	{
		int i, j;

		int dDis, dShortest;
		int iXDis, iYDis;
		int iX=0, iY=0;

		dShortest=1000;

		// find the nearest dot
		for (i=0; i < Maze.HEIGHT; i++)
			for (j=0; j < Maze.WIDTH; j++)
			{
				if (maze.iMaze[i][j]==Maze.DOT)
				{
					iXDis=j*16-8-pacman.iX;
					iYDis=i*16-8-pacman.iY;
					dDis=iXDis*iXDis+iYDis*iYDis;

					if (dDis<dShortest)
					{
						dShortest=dDis;

						iX=iXDis; iY=iYDis;
					}
				}	
			}

		// now iX and iY is the goal (relative position)

		int iFact=100000;

		if (iX > 0)
		{
			iDirScore[Resources.RIGHT] += iFact;
		}
		else if (iX < 0)
		{
			iDirScore[Resources.LEFT] += iFact;
		}

		if (iY > 0)
		{
			iDirScore[Resources.DOWN] += iFact;
		}
		else if (iY<0)
		{
			iDirScore[Resources.UP] += iFact;
		}
	}

	void addPowerDotScore()
	{
		// TODO
	}
	
	
}
