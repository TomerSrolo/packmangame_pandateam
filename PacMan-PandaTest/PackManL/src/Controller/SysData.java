/**
 * Panda PacMan
 * 
 */
package Controller;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import View.MainFrame;

/**
 * Will read JSON from here 
 */
public class SysData 
{
	static ArrayList<MainFrame> gameFrames;
	private int turn = 1;
	// static variable single_instance of type Singleton 
    private static SysData SysData = null; 
  
    // private constructor restricted to this class itself 
    private SysData() 
    { 
    	gameFrames = new ArrayList<>(); 
    } 
  
    // static method to create instance of Singleton class 
    public static SysData getInstance() 
    { 
        if (SysData == null) 
        	SysData = new SysData(); 
  
        return SysData; 
    } 

    public void OnePlayerGame()
	{
    	gameFrames.add(new MainFrame("panda"));
	}
    
	public void TwoPlayersGame()
	{
		gameFrames.add(new MainFrame("one"));
		gameFrames.add(new MainFrame("two"));
		
		gameFrames.get(0).setVisible(false);
		gameFrames.get(1).setVisible(false);
		
		
		/*Thread turnsThread = new Thread()
		{
		    public void run() 
		    {
		    	long curT = System.currentTimeMillis();
		    	
		        while(true)
		        {
		        	if(curT - System.currentTimeMillis() == 40000)
		        	{
		        		System.out.println("hi");
		        		turn++;
		        		curT = System.currentTimeMillis();
		        	}
		        }
		    }
		};
		
		Thread switchThread = new Thread()
		{
		    public void run() 
		    {
		        while(true)
		        {
		        	gameFrames.get(turn%2).setVisible(true);
		        	gameFrames.get(turn%2-1).setVisible(false);
		        }
		    }
		};
		
		turnsThread.start();
		switchThread.start();*/
	}
}
