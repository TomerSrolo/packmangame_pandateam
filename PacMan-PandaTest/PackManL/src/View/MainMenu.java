package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import Controller.SysData;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class MainMenu extends JFrame 
{

	private JPanel MainPanel;
	public static JPanel MenuPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					try {
					    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					        if ("Nimbus".equals(info.getName())) {
					            UIManager.setLookAndFeel(info.getClassName());
					            break;
					        }
					    }
					} catch (Exception e) {
					    
					}
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainMenu() 
	{	
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1336, 720);
		MainPanel = new JPanel();
		MainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(MainPanel);
		MainPanel.setLayout(null);
		
		JPanel TopPanel = new JPanel();
		TopPanel.setBackground(Color.BLACK);
		TopPanel.setBounds(0, 0, 1330, 162);
		MainPanel.add(TopPanel);
		
		MenuPanel = new JPanel();
		MenuPanel.setBounds(0, 162, 1330, 523);
		MainPanel.add(MenuPanel);
		MenuPanel.setLayout(null);
		
		
		JPanel CenPanel = new JPanel();
		CenPanel.setBounds(0, 0, 1330, 441);
		MenuPanel.add(CenPanel);
		CenPanel.setBackground(Color.BLACK);
		CenPanel.setLayout(null);
		
		JPanel OnePanel = new JPanel();
		OnePanel.setBackground(Color.BLACK);
		OnePanel.setBounds(0, 13, 1330, 70);
		CenPanel.add(OnePanel);
		OnePanel.setLayout(null);
		
		JLabel OneLbl = new JLabel("ONE PLAYER GAME");
		OneLbl.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 30));
		OneLbl.setBounds(502, 13, 313, 44);
		OneLbl.setForeground(Color.WHITE);
		OnePanel.add(OneLbl);
		
		JPanel TwoPanel = new JPanel();
		TwoPanel.setBackground(Color.BLACK);
		TwoPanel.setBounds(0, 96, 1330, 70);
		CenPanel.add(TwoPanel);
		TwoPanel.setLayout(null);
		
		JLabel TwoLbl = new JLabel("TWO PLAYERS GAME");
		TwoLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 30));
		TwoLbl.setBounds(490, 13, 368, 44);
		TwoLbl.setForeground(Color.WHITE);
		TwoPanel.add(TwoLbl);
		
		JPanel ScoresPanel = new JPanel();
		ScoresPanel.setBackground(Color.BLACK);
		ScoresPanel.setBounds(0, 179, 1330, 70);
		CenPanel.add(ScoresPanel);
		ScoresPanel.setLayout(null);
		
		JLabel ScoreLbl = new JLabel("TOP 10 SCORES");
		ScoreLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 30));
		ScoreLbl.setBounds(543, 13, 277, 44);
		ScoreLbl.setForeground(Color.WHITE);
		ScoresPanel.add(ScoreLbl);
		
		JPanel QuestionsPanel = new JPanel();
		QuestionsPanel.setBackground(Color.BLACK);
		QuestionsPanel.setBounds(0, 266, 1330, 70);
		CenPanel.add(QuestionsPanel);
		QuestionsPanel.setLayout(null);
		
		JLabel QuestionsLbl = new JLabel("MANAGE QUESTIONS");
		QuestionsLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 30));
		QuestionsLbl.setBounds(475, 13, 335, 44);
		QuestionsLbl.setForeground(Color.WHITE);
		QuestionsPanel.add(QuestionsLbl);
		
		JPanel ExitPanel = new JPanel();
		ExitPanel.setBackground(Color.BLACK);
		ExitPanel.setBounds(0, 349, 1330, 70);
		CenPanel.add(ExitPanel);
		ExitPanel.setLayout(null);
		
		JLabel ExitLbl = new JLabel("EXIT");
		ExitLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 30));
		ExitLbl.setBounds(627, 13, 82, 44);
		ExitLbl.setForeground(Color.WHITE);
		ExitPanel.add(ExitLbl);
		
		JPanel ButPanel = new JPanel();
		ButPanel.setBounds(0, 441, 1330, 82);
		MenuPanel.add(ButPanel);
		ButPanel.setBackground(Color.BLACK);
		
        ButPanel.setLayout(new BorderLayout(0, 0));
        
        TopPanel.setLayout(new BorderLayout(0, 0));

		final ImageIcon pacman = new ImageIcon(this.getClass().getResource("Pac-Man-trans-back.jpg").getPath());
        final ImageIcon pacmanscaledImage = new ImageIcon(pacman.getImage()
                .getScaledInstance(pacman.getIconWidth(),
                		pacman.getIconHeight() / 2, Image.SCALE_SMOOTH));
        final JLabel pacLbl = new JLabel(pacmanscaledImage);
		TopPanel.add(pacLbl,BorderLayout.CENTER);
		
		final ImageIcon panda = new ImageIcon(this.getClass().getResource("panda.jpg").getPath());
        final ImageIcon pandascaledImage = new ImageIcon(panda.getImage()
                .getScaledInstance(panda.getIconWidth() / 2,
                		panda.getIconHeight() / 2, Image.SCALE_SMOOTH));
        final JLabel pandaLbl = new JLabel(pandascaledImage);
		TopPanel.add(pandaLbl,BorderLayout.WEST);
		
		final ImageIcon pandamir = new ImageIcon(this.getClass().getResource("pandamir.jpg").getPath());
        final ImageIcon pandamirscaledImage = new ImageIcon(pandamir.getImage()
                .getScaledInstance(pandamir.getIconWidth() / 2,
                		pandamir.getIconHeight() / 2, Image.SCALE_SMOOTH));
        final JLabel pandaMirLbl = new JLabel(pandamirscaledImage);
		TopPanel.add(pandaMirLbl,BorderLayout.EAST);
		
		TopPanel.repaint();
		
		final ImageIcon pacani = new ImageIcon(this.getClass().getResource("200.gif").getPath());
		
		//Place the frame in the center
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
        final JLabel pacaniLbl = new JLabel(pacani);
        ButPanel.add(pacaniLbl,BorderLayout.CENTER);
        
        JPanel ManageQuestPanel = ManageQuestionsPanel.getInstance();
        ManageQuestPanel.setBackground(Color.BLACK);
        MainPanel.add(ManageQuestPanel);
        ManageQuestPanel.setBounds(0, 162, 1330, 523);
        ManageQuestPanel.setLayout(null);
        
		ManageQuestPanel.setVisible(false);
		MenuPanel.setVisible(true);
		
        ButPanel.repaint();
		
		OnePanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				SysData.getInstance().OnePlayerGame();
			}
		});
		
		TwoPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				//SysData.getInstance().TwoPlayersGame();
			}
		});
		
		ScoresPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				
			}
		});
		
		QuestionsPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				ManageQuestPanel.setVisible(true);
				MenuPanel.setVisible(false);
			}
		});
		
		ExitPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				System.exit(0);
			}
		});
		
		MenuPanelHover(OnePanel, OneLbl);
		MenuPanelHover(TwoPanel, TwoLbl);
		MenuPanelHover(ScoresPanel, ScoreLbl);
		MenuPanelHover(QuestionsPanel, QuestionsLbl);
		MenuPanelHover(ExitPanel, ExitLbl);
		
	}
	
	public void MenuPanelHover(JPanel objPanel,JLabel objLbl)
	{
		objPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				objPanel.setBackground(Color.YELLOW);
				objLbl.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 30));
				objLbl.setForeground(Color.BLACK);
			}
			@Override
			public void mouseExited(MouseEvent e) 
			{
				objPanel.setBackground(Color.BLACK);
				objLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 30));
				objLbl.setForeground(Color.WHITE);
			}
		});
	}
}
