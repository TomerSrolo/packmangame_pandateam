package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.QuestionCandy;

import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class QuestionFrame extends JFrame {

	private JPanel contentPane;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public QuestionFrame(QuestionCandy question) {
		
		
		
		setVisible(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 385, 195);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel TopPanel = new JPanel();
		TopPanel.setBackground(Color.gray);
		TopPanel.setBounds(0, 0, 385, 100);
		contentPane.add(TopPanel);
		TopPanel.setLayout(null);
		
		JTextArea txtrQuestionNumber = new JTextArea();
		txtrQuestionNumber.setText("Question number 1:  "
				+ "Press S in the game to Continue");
		txtrQuestionNumber.setBounds(10, 11, 350, 78);
		txtrQuestionNumber.setEditable(false);
		TopPanel.add(txtrQuestionNumber);
		
		JPanel ButtonPanel = new JPanel();
		ButtonPanel.setBackground(Color.gray);
		ButtonPanel.setBounds(0, 100, 370, 55);
		contentPane.add(ButtonPanel);
		ButtonPanel.setLayout(null);
		
		JButton btnContinue = new JButton("Continue");
		btnContinue.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				question.setStartGhosts(true);
				setVisible(false);
							
				
			}
		});
		
		btnContinue.setBounds(141, 11, 89, 23);
		ButtonPanel.add(btnContinue);
		
		
		
		
		
	}
}
