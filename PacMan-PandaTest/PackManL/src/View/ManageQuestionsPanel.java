package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import Controller.SysData;

public class ManageQuestionsPanel extends JPanel 
{
	private static ManageQuestionsPanel QuestPanel = null; 
	
    JPanel selectQuestPanel;
    JLabel QuestionSelLbl;
    JComboBox QuestionsSelBox;
    JPanel newQuestPanel;
    JLabel lblComposeAQuestion;
    JLabel lblCancel;
    JPanel CancelBut;
    JTextArea newQuestArea;
    
	JPanel EditBut1;
	JPanel EditBut2;
	JPanel EditBut3;
	JPanel EditBut4;
	JPanel EditBut5;
	
	JLabel EditLbl1;
	JLabel EditLbl2;
	JLabel EditLbl3;
	JLabel EditLbl4;
	JLabel EditLbl5;
	
	JPanel DeleteBut1;
	JPanel DeleteBut2;
	JPanel DeleteBut3;
	JPanel DeleteBut4;
	JPanel DeleteBut5;
	
	JLabel DeleteLbl1;
	JLabel DeleteLbl2;
	JLabel DeleteLbl3;
	JLabel DeleteLbl4;
	JLabel DeleteLbl5;
	
	JLabel ans1tag;
	JLabel ans2tag;
	JLabel ans3tag;
	JLabel ans4tag;

	JLabel ans1Lbl;
	JLabel ans2Lbl;
	JLabel ans3Lbl;
	JLabel ans4Lbl;
	
	JTextArea editAns1;
	JTextArea editAns2;
	JTextArea editAns3;
	JTextArea editAns4;
	
    JLabel lblCurrentRightAnswer;
    JLabel lblQuestionDifficulty;
    JComboBox diffBox;
    JComboBox answerBox;
    JLabel lblCurrent;
    
    JPanel newPanel;
    JLabel NewQuestionLbl;
    
    // static method to create instance of Singleton class 
    public static ManageQuestionsPanel getInstance() 
    { 
        if (QuestPanel == null) 
        	QuestPanel = new ManageQuestionsPanel(); 
  
        return QuestPanel; 
    } 
    
	/**
	 * Create the panel.
	 * private constructor restricted to this class itself 
	 */
	private ManageQuestionsPanel() 
	{		
		//init question managment panel
		//JPanel ManageQuestPanel = new JPanel();
        //ManageQuestPanel.setBackground(Color.LIGHT_GRAY);
       // this.add(ManageQuestPanel);
        //ManageQuestPanel.setBounds(0, 162, 1330, 523);
       // ManageQuestPanel.setLayout(null);
        
        //init inner panel - question answers and buttons
        JPanel PrimePanel = new JPanel();
        PrimePanel.setBackground(Color.BLACK);
        PrimePanel.setBounds(0, 0, 1330, 75);
        this.add(PrimePanel);
        PrimePanel.setLayout(null);
        
        selectQuestPanel = new JPanel();
        selectQuestPanel.setBackground(Color.BLACK);
        selectQuestPanel.setBounds(12, 0, 1306, 75);
        PrimePanel.add(selectQuestPanel);
        selectQuestPanel.setLayout(null);
        
        QuestionSelLbl = new JLabel("Select a question:");
        QuestionSelLbl.setBounds(0, 0, 263, 75);
        selectQuestPanel.add(QuestionSelLbl);
        QuestionSelLbl.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 30));
        QuestionSelLbl.setForeground(Color.WHITE);
        
        QuestionsSelBox = new JComboBox();
        QuestionsSelBox.setBounds(275, 13, 1031, 49);
        selectQuestPanel.add(QuestionsSelBox);
        QuestionsSelBox.setForeground(Color.WHITE);
        QuestionsSelBox.setBackground(Color.WHITE);
        QuestionsSelBox.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        
        newQuestPanel = new JPanel();
        newQuestPanel.setLayout(null);
        newQuestPanel.setBackground(Color.BLACK);
        newQuestPanel.setBounds(0, 0, 1330, 75);
        PrimePanel.add(newQuestPanel);
        
        lblComposeAQuestion = new JLabel("New question:");
        lblComposeAQuestion.setForeground(Color.WHITE);
        lblComposeAQuestion.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 30));
        lblComposeAQuestion.setBounds(0, 0, 305, 75);
        newQuestPanel.add(lblComposeAQuestion);
        
        CancelBut = new JPanel();
        CancelBut.setLayout(null);
        CancelBut.setBackground(Color.BLACK);
        CancelBut.setBounds(1210, 0, 120, 75);
        newQuestPanel.add(CancelBut);
        
        lblCancel = new JLabel("Cancel");
        lblCancel.setForeground(Color.WHITE);
        lblCancel.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        lblCancel.setBounds(16, 13, 92, 49);
        CancelBut.add(lblCancel);
        
        newQuestArea = new JTextArea();
        newQuestArea.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 14));
        newQuestArea.setBounds(212, 13, 986, 49);
        newQuestPanel.add(newQuestArea);
        
        JPanel FirstPanel = new JPanel();
        FirstPanel.setBorder(null);
        FirstPanel.setBackground(Color.DARK_GRAY);
        FirstPanel.setBounds(0, 75, 1330, 75);
        this.add(FirstPanel);
        FirstPanel.setLayout(null);
        
        JPanel SecPanel = new JPanel();
        SecPanel.setBackground(Color.BLACK);
        SecPanel.setBounds(0, 150, 1330, 75);
        this.add(SecPanel);
        SecPanel.setLayout(null);
        
        JPanel ThirdPanel = new JPanel();
        ThirdPanel.setBackground(Color.DARK_GRAY);
        ThirdPanel.setBounds(0, 225, 1330, 75);
        this.add(ThirdPanel);
        ThirdPanel.setLayout(null);
        
        JPanel FourthPanel = new JPanel();
        FourthPanel.setBackground(Color.BLACK);
        FourthPanel.setBounds(0, 300, 1330, 75);
        this.add(FourthPanel);
        FourthPanel.setLayout(null);
        
        JPanel FifthPanel = new JPanel();
        FifthPanel.setBackground(Color.DARK_GRAY);
        FifthPanel.setBounds(0, 375, 1330, 75);
        this.add(FifthPanel);
        FifthPanel.setLayout(null);
        
        JPanel QuestButPanel = new JPanel();
        QuestButPanel.setBorder(new LineBorder(Color.WHITE, 0, true));
        QuestButPanel.setBackground(Color.BLACK);
        QuestButPanel.setBounds(0, 448, 1330, 75);
        this.add(QuestButPanel);
        QuestButPanel.setLayout(null);
        
        JPanel backBut = new JPanel();
        backBut.setBackground(Color.BLACK);
        backBut.setBounds(0, 0, 199, 75);
        QuestButPanel.add(backBut);
        backBut.setLayout(null);
        
        JLabel BackLbl = new JLabel("<- Back");
        BackLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 28));
        BackLbl.setForeground(Color.WHITE);
        BackLbl.setBounds(12, 13, 154, 57);
        backBut.add(BackLbl);
        
        //init manage answers panels
        JPanel AnsOptPanel1 = new JPanel();
        AnsOptPanel1.setBackground(Color.DARK_GRAY);
        AnsOptPanel1.setBounds(1090, 0, 240, 75);
        FirstPanel.add(AnsOptPanel1);
        AnsOptPanel1.setLayout(null);
        
        JPanel AnsOptPanel2 = new JPanel();
        AnsOptPanel2.setBackground(Color.BLACK);
        AnsOptPanel2.setBounds(1090, 0, 240, 75);
        SecPanel.add(AnsOptPanel2);
        AnsOptPanel2.setLayout(null);
        
        JPanel AnsOptPanel3 = new JPanel();
        AnsOptPanel3.setBackground(Color.DARK_GRAY);
        AnsOptPanel3.setBounds(1090, 0, 240, 75);
        ThirdPanel.add(AnsOptPanel3);
        AnsOptPanel3.setLayout(null);
        
        JPanel AnsOptPanel4 = new JPanel();
        AnsOptPanel4.setBackground(Color.BLACK);
        AnsOptPanel4.setBounds(1090, 0, 240, 75);
        FourthPanel.add(AnsOptPanel4);
        AnsOptPanel4.setLayout(null);
        
        JPanel AnsOptPanel5 = new JPanel();
        AnsOptPanel5.setBackground(Color.DARK_GRAY);
        AnsOptPanel5.setBounds(1090, 0, 240, 75);
        FifthPanel.add(AnsOptPanel5);
        AnsOptPanel5.setLayout(null);
        
        //init panel buttons at the manage answers panels
        
        //edit panels and lables
        EditBut1 = new JPanel();
        EditBut1.setBounds(0, 0, 120, 75);
        AnsOptPanel1.add(EditBut1);
        EditBut1.setBackground(Color.DARK_GRAY);
        EditBut1.setLayout(null);
        
        EditLbl1 = new JLabel("Edit");
        EditLbl1.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        EditLbl1.setForeground(Color.WHITE);
        EditLbl1.setBounds(32, 13, 76, 44);
        EditBut1.add(EditLbl1);
        
        EditBut2 = new JPanel();
        EditBut2.setBounds(0, 0, 120, 75);
        AnsOptPanel2.add(EditBut2);
        EditBut2.setLayout(null);
        EditBut2.setBackground(Color.BLACK);
        
        EditLbl2 = new JLabel("Edit");
        EditLbl2.setForeground(Color.WHITE);
        EditLbl2.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        EditLbl2.setBounds(32, 13, 76, 44);
        EditBut2.add(EditLbl2);
        
        EditBut3 = new JPanel();
        EditBut3.setBounds(0, 0, 120, 75);
        AnsOptPanel3.add(EditBut3);
        EditBut3.setLayout(null);
        EditBut3.setBackground(Color.DARK_GRAY);
        
        EditLbl3 = new JLabel("Edit");
        EditLbl3.setForeground(Color.WHITE);
        EditLbl3.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        EditLbl3.setBounds(32, 13, 76, 44);
        EditBut3.add(EditLbl3);
        
        EditBut4 = new JPanel();
        EditBut4.setBounds(0, 0, 120, 75);
        AnsOptPanel4.add(EditBut4);
        EditBut4.setLayout(null);
        EditBut4.setBackground(Color.BLACK);
        
        EditLbl4 = new JLabel("Edit");
        EditLbl4.setForeground(Color.WHITE);
        EditLbl4.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        EditLbl4.setBounds(32, 13, 76, 44);
        EditBut4.add(EditLbl4);
        
        EditBut5 = new JPanel();
        EditBut5.setBounds(0, 0, 120, 75);
        AnsOptPanel5.add(EditBut5);
        EditBut5.setLayout(null);
        EditBut5.setBackground(Color.DARK_GRAY);
        
        EditLbl5 = new JLabel("Edit");
        EditLbl5.setForeground(Color.WHITE);
        EditLbl5.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        EditLbl5.setBounds(32, 13, 76, 44);
        EditBut5.add(EditLbl5);
        
        //delete panels and labels
        DeleteBut1 = new JPanel();
        DeleteBut1.setBounds(120, 0, 120, 75);
        AnsOptPanel1.add(DeleteBut1);
        DeleteBut1.setBackground(Color.DARK_GRAY);
        DeleteBut1.setLayout(null);
        
        DeleteLbl1 = new JLabel("Delete");
        DeleteLbl1.setBounds(16, 13, 92, 49);
        DeleteLbl1.setForeground(Color.WHITE);
        DeleteLbl1.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        DeleteBut1.add(DeleteLbl1);
        
        DeleteBut2 = new JPanel();
        DeleteBut2.setBounds(120, 0, 120, 75);
        AnsOptPanel2.add(DeleteBut2);
        DeleteBut2.setBackground(Color.BLACK);
        DeleteBut2.setLayout(null);
        
        DeleteLbl2 = new JLabel("Delete");
        DeleteLbl2.setBounds(16, 13, 92, 49);
        DeleteLbl2.setForeground(Color.WHITE);
        DeleteLbl2.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        DeleteBut2.add(DeleteLbl2);
        
        DeleteBut3 = new JPanel();
        DeleteBut3.setBounds(120, 0, 120, 75);
        AnsOptPanel3.add(DeleteBut3);
        DeleteBut3.setBackground(Color.DARK_GRAY);
        DeleteBut3.setLayout(null);
        
        DeleteLbl3 = new JLabel("Delete");
        DeleteLbl3.setBounds(16, 13, 92, 49);
        DeleteLbl3.setForeground(Color.WHITE);
        DeleteLbl3.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        DeleteBut3.add(DeleteLbl3);
               
        DeleteBut4 = new JPanel();
        DeleteBut4.setBounds(120, 0, 120, 75);
        AnsOptPanel4.add(DeleteBut4);
        DeleteBut4.setBackground(Color.BLACK);
        DeleteBut4.setLayout(null);
        
        DeleteLbl4 = new JLabel("Delete");
        DeleteLbl4.setBounds(16, 13, 92, 49);
        DeleteLbl4.setForeground(Color.WHITE);
        DeleteLbl4.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        DeleteBut4.add(DeleteLbl4);
        
        DeleteBut5 = new JPanel();
        DeleteBut5.setBounds(120, 0, 120, 75);
        AnsOptPanel5.add(DeleteBut5);
        DeleteBut5.setBackground(Color.DARK_GRAY);
        DeleteBut5.setLayout(null);
        
        DeleteLbl5 = new JLabel("Delete");
        DeleteLbl5.setBounds(16, 13, 92, 49);
        DeleteLbl5.setForeground(Color.WHITE);
        DeleteLbl5.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        DeleteBut5.add(DeleteLbl5);
        
        lblCurrentRightAnswer = new JLabel("Correct answer:");
        lblCurrentRightAnswer.setForeground(Color.WHITE);
        lblCurrentRightAnswer.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 30));
        lblCurrentRightAnswer.setBounds(201, 0, 240, 75);
        FifthPanel.add(lblCurrentRightAnswer);
        
        lblQuestionDifficulty = new JLabel("Question difficulty:");
        lblQuestionDifficulty.setForeground(Color.WHITE);
        lblQuestionDifficulty.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 30));
        lblQuestionDifficulty.setBounds(599, 0, 275, 75);
        FifthPanel.add(lblQuestionDifficulty);
        
        answerBox = new JComboBox();
        answerBox.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 26));
        answerBox.setBounds(435, 13, 83, 49);
        FifthPanel.add(answerBox);
        
        diffBox = new JComboBox();
        diffBox.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 26));
        diffBox.setBounds(886, 13, 83, 49);
        FifthPanel.add(diffBox);
        
        lblCurrent = new JLabel("Current:");
        lblCurrent.setForeground(Color.WHITE);
        lblCurrent.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 30));
        lblCurrent.setBounds(12, 0, 240, 75);
        FifthPanel.add(lblCurrent);
        
        ans1tag = new JLabel("1.");
        ans1tag.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 40));
        ans1tag.setBackground(Color.DARK_GRAY);
        ans1tag.setForeground(Color.WHITE);
        ans1tag.setBounds(12, 0, 37, 75);
        FirstPanel.add(ans1tag);
        
        editAns1 = new JTextArea();
        editAns1.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        editAns1.setBounds(47, 13, 1030, 49);
        FirstPanel.add(editAns1);
               
        ans2tag = new JLabel("2.");
        ans2tag.setForeground(Color.WHITE);
        ans2tag.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 40));
        ans2tag.setBackground(Color.DARK_GRAY);
        ans2tag.setBounds(12, 0, 36, 75);
        SecPanel.add(ans2tag);
        
        editAns2 = new JTextArea();
        editAns2.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        editAns2.setBounds(48, 13, 1030, 49);
        SecPanel.add(editAns2);
        
        ans3tag = new JLabel("3.");
        ans3tag.setForeground(Color.WHITE);
        ans3tag.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 40));
        ans3tag.setBackground(Color.DARK_GRAY);
        ans3tag.setBounds(12, 0, 36, 75);
        ThirdPanel.add(ans3tag);
        
        editAns3 = new JTextArea();
        editAns3.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        editAns3.setBounds(48, 13, 1030, 49);
        ThirdPanel.add(editAns3);
        
        ans4tag = new JLabel("4.");
        ans4tag.setForeground(Color.WHITE);
        ans4tag.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 40));
        ans4tag.setBackground(Color.DARK_GRAY);
        ans4tag.setBounds(12, 0, 37, 75);
        FourthPanel.add(ans4tag);
        
        ans1Lbl = new JLabel();
        ans1Lbl.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        ans1Lbl.setForeground(Color.WHITE);
        ans1Lbl.setBounds(50, 13, 1028, 49);
        FirstPanel.add(ans1Lbl);
        
        ans2Lbl = new JLabel();
        ans2Lbl.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        ans2Lbl.setForeground(Color.WHITE);
        ans2Lbl.setBounds(50, 13, 1028, 49);
        SecPanel.add(ans2Lbl);
        
        ans3Lbl = new JLabel();
        ans3Lbl.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        ans3Lbl.setForeground(Color.WHITE);
        ans3Lbl.setBounds(50, 13, 1028, 49);
        ThirdPanel.add(ans3Lbl);
        
        ans4Lbl = new JLabel();
        ans4Lbl.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
        ans4Lbl.setForeground(Color.WHITE);
        ans4Lbl.setBounds(50, 13, 1028, 49);
        FourthPanel.add(ans4Lbl);
        
        editAns4 = new JTextArea();
        editAns4.setBounds(50, 13, 1030, 49);
        FourthPanel.add(editAns4);
        editAns4.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 16));
               
        allNew();
        FifthPanelHandler(EditBut5, EditLbl5, DeleteBut5, DeleteLbl5,answerBox,diffBox,lblCurrent);
        
        EnterToYellow(backBut, BackLbl);
        ExitToBlack(backBut, BackLbl);
        
        newPanel = new JPanel();
        NewQuestionLbl = new JLabel("New question");
        NewQuestionLbl.setForeground(Color.WHITE);
        NewQuestionLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
        NewQuestionLbl.setBounds(27, 13, 201, 49);
        newPanel.add(NewQuestionLbl);

        CancelBut.addMouseListener(new MouseAdapter() 
        {
        	@Override
        	public void mouseClicked(MouseEvent e) 
        	{
        		selectQuestPanel.setVisible(true);
        		newQuestPanel.setVisible(false);
        	}
        });
        
        newPanel.setLayout(null);
        newPanel.setBackground(Color.BLACK);
        newPanel.setBounds(1090, 0, 240, 75);
        QuestButPanel.add(newPanel);
        
        
        newQuestionHandler();
        initQuestionBox(QuestionsSelBox);
        
        backBut.addMouseListener(new MouseAdapter() 
        {
        	@Override
        	public void mouseClicked(MouseEvent e) 
        	{
        		ManageQuestionsPanel.getInstance().setVisible(false);
        		MainMenu.MenuPanel.setVisible(true);
        	}
        });
        
	}
	
	public void allNew()
	{
		
        changeToAdd(EditBut1, EditLbl1, DeleteBut1, DeleteLbl1,ans1tag,ans1Lbl,editAns1);
        changeToAdd(EditBut2, EditLbl2, DeleteBut2, DeleteLbl2,ans2tag,ans2Lbl,editAns2);
        changeToAdd(EditBut3, EditLbl3, DeleteBut3, DeleteLbl3,ans3tag,ans3Lbl,editAns3);
        changeToAdd(EditBut4, EditLbl4, DeleteBut4, DeleteLbl4,ans4tag,ans4Lbl,editAns4);
	}
	
	public void newQuestionHandler()
	{
		NewQuestionLbl.setText("New question");
		selectQuestPanel.setVisible(true);
		newQuestPanel.setVisible(false);
		
		removeMouseListeners(newPanel);
		mouseHoverBut();
		
		newPanel.addMouseListener(new MouseAdapter() 
        {
        	@Override
        	public void mouseClicked(MouseEvent e) 
        	{
        		createNewQuestion();
        		allNew();
        	}
        });
	}
	
	public void createNewQuestion()
	{
		selectQuestPanel.setVisible(false);
		newQuestPanel.setVisible(true);
		NewQuestionLbl.setText("Save question");
		
		removeMouseListeners(newPanel);
		mouseHoverBut();
		
		newPanel.addMouseListener(new MouseAdapter() 
        {
        	@Override
        	public void mouseClicked(MouseEvent e) 
        	{
        		newQuestionHandler();
        	}
        });
	}
	
	public void FifthPanelHandler(JPanel editbut,JLabel editlbl,JPanel deletePanel,JLabel deletelbl,JComboBox answerBox,JComboBox diffBox,JLabel curr)
	{
		curr.setText("Current:");
		editbut.setVisible(false);
		deletelbl.setText("Edit");
		answerBox.setEnabled(false);
		diffBox.setEnabled(false);
		initBox1to4(answerBox);
        initBox1to4(diffBox);
        
		removeMouseListeners(deletePanel);
		removeMouseListeners(editbut);
		mouseHoverBut();
		
        deletePanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				FifthPanelEdit(editbut, editlbl, deletePanel, deletelbl,answerBox,diffBox,curr);
			}
		});
	}
	
	public void FifthPanelEdit(JPanel editbut,JLabel editlbl,JPanel deletePanel,JLabel deletelbl,JComboBox answerBox,JComboBox diffBox,JLabel curr)
	{
		String corAns = answerBox.getSelectedItem().toString();
		String curDif = diffBox.getSelectedItem().toString();
		curr.setText("Edit Mode:");
		editbut.setVisible(true);
		editlbl.setText("Save");
		deletelbl.setText("Cancel");
		answerBox.setEnabled(true);
		diffBox.setEnabled(true);
		
		removeMouseListeners(deletePanel);
		removeMouseListeners(editbut);
		mouseHoverBut();
		
		deletePanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				answerBox.setSelectedItem(corAns);
				diffBox.setSelectedItem(curDif);
				FifthPanelHandler(editbut, editlbl, deletePanel, deletelbl,answerBox,diffBox,curr);
			}
		});
		
		editlbl.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				FifthPanelHandler(editbut, editlbl, deletePanel, deletelbl,answerBox,diffBox,curr);
			}
		});
	}
	public void initBox1to4(JComboBox<String> box)
	{
		if(box.getItemCount()!=4)
		{
		box.addItem("1");
		box.addItem("2");
		box.addItem("3");
		box.addItem("4");
		}
	}
	
	public void initQuestionBox(JComboBox<String> box)
	{
		box.addItem("what day is it?");
		box.addItem("is it friday?");
		box.addItem("is it sunday?");
		box.addItem("is it wensday?");
		box.addActionListener(new ActionListener() 
        {
        	public void actionPerformed(ActionEvent arg0) 
        	{
        		/*read answers for question selected*/
        		int n = JOptionPane.showConfirmDialog(
					    null,
					    box.getSelectedItem().toString(),
					    "An Inane Question",
					    JOptionPane.YES_NO_OPTION);
        	}
        });
	}
	
	public void EnterToYellow(JPanel objPanel,JLabel objLbl)
	{
		if(objPanel != null && objLbl != null)
		objPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseEntered(MouseEvent e) 
			{
				objPanel.setBackground(Color.YELLOW);
				objLbl.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 26));
				objLbl.setForeground(Color.BLACK);
			}
		});
	}
	
	public void ExitToBlack(JPanel objPanel,JLabel objLbl)
	{
		EnterToYellow(objPanel, objLbl);
		if(objPanel != null && objLbl != null)
		objPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseExited(MouseEvent e) 
			{
				objPanel.setBackground(Color.BLACK);
				objLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
				objLbl.setForeground(Color.WHITE);
			}
		});
	}
	
	public void mouseHoverBut()
	{
		ExitToDarkGray(DeleteBut1, DeleteLbl1);
		ExitToDarkGray(DeleteBut3, DeleteLbl3);
		ExitToDarkGray(DeleteBut5, DeleteLbl5);
		
		ExitToDarkGray(EditBut1, EditLbl1);
		ExitToDarkGray(EditBut3, EditLbl3);
		ExitToDarkGray(EditBut5, EditLbl5);
		
		ExitToBlack(DeleteBut2, DeleteLbl2);
		ExitToBlack(DeleteBut4, DeleteLbl4);
		ExitToBlack(EditBut2, EditLbl2);
		ExitToBlack(EditBut4, EditLbl4);
		
		ExitToBlack(newPanel, NewQuestionLbl);
		ExitToBlack(CancelBut, lblCancel);
	}
	
	public void ExitToDarkGray(JPanel objPanel,JLabel objLbl)
	{
		EnterToYellow(objPanel, objLbl);
		if(objPanel != null && objLbl != null)
		objPanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseExited(MouseEvent e) 
			{
				objPanel.setBackground(Color.DARK_GRAY);
				objLbl.setFont(new Font("Microsoft JhengHei UI Light", Font.BOLD, 26));
				objLbl.setForeground(Color.WHITE);
			}
		});
	}
	
	public void changeToAdd(JPanel editbut,JLabel editlbl,JPanel deletePanel,JLabel deletelbl,JLabel tag,JLabel ans,JTextArea editAns)
	{
		ans.setText("");
		editAns.setText("");
		tag.setVisible(false);
		editbut.setVisible(false);
		ans.setVisible(false);
		editAns.setVisible(false);
		deletelbl.setText("Add");
		
		removeMouseListeners(deletePanel);
		removeMouseListeners(editbut);
		mouseHoverBut();
		
		deletePanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				changeToEditAndDelete(editbut,editlbl,deletePanel,deletelbl,tag,ans,editAns);
			}
		});
		
	}
	
	public void changeToEditAndDelete(JPanel editbut,JLabel editlbl,JPanel deletePanel,JLabel deletelbl,JLabel tag,JLabel ans,JTextArea editAns)
	{
		editbut.setVisible(true);
		tag.setVisible(true);
		editbut.setVisible(true);
		ans.setVisible(true);
		editAns.setVisible(false);
		editlbl.setText("Edit");
		deletelbl.setText("Delete");
		
		removeMouseListeners(deletePanel);
		removeMouseListeners(editbut);
		mouseHoverBut();
		
		deletePanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(!ans.getText().trim().isEmpty())
				{
					int n = JOptionPane.showConfirmDialog(
						    ManageQuestionsPanel.getInstance(),
						    "You are about to delete asnwer number "+ tag.getText()+ " \ndo you want to continue?\n(answer will not change in the json file until you press write to file)",
						    "An Inane Question",
						    JOptionPane.YES_NO_OPTION);
					if(n==0)
					{
						changeToAdd(editbut,editlbl,deletePanel,deletelbl,tag,ans,editAns);
					}
				}
				else
				{
					changeToAdd(editbut,editlbl,deletePanel,deletelbl,tag,ans,editAns);
				}
			}
		});
		
		editbut.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if(!ans.getText().isEmpty())
				editAns.setText(ans.getText().substring(6,ans.getText().length()-7));
				changeToSaveandCancel(editbut,editlbl,deletePanel,deletelbl,tag,ans,editAns);
			}
		});
	}
	
	public void changeToSaveandCancel(JPanel editbut,JLabel editlbl,JPanel deletePanel,JLabel deletelbl,JLabel tag,JLabel ans,JTextArea editAns)
	{
		editlbl.setText("Save");
		deletelbl.setText("Cancel");
		ans.setVisible(false);
		editAns.setVisible(true);
		
		removeMouseListeners(deletePanel);
		removeMouseListeners(editbut);
		mouseHoverBut();
		
		deletePanel.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				changeToEditAndDelete(editbut,editlbl,deletePanel,deletelbl,tag,ans,editAns);
			}
		});
		
		editbut.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				ans.setText("<html>" + editAns.getText() + "</html>");
				changeToEditAndDelete(editbut,editlbl,deletePanel,deletelbl,tag,ans,editAns);
			}
		});
	}

	public void removeMouseListeners(JPanel panel)
	{
		MouseListener[] mouseListeners = panel.getMouseListeners();
		for (MouseListener mouseListener : mouseListeners) 
		{
			panel.removeMouseListener(mouseListener);
		}
	}
}

