/**
 * Panda PacMan
 * 
 */

package View;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

/**
 * a java applet class for pacman
 */
public class PacmanApplet extends Applet// should be in model
implements ActionListener
{
	private static final long serialVersionUID = -749993332452315528L;

	static MainFrame pacMan=null;

	public void init()
	{
		setSize(100,100);
		// create button
		setLayout(new FlowLayout(FlowLayout.CENTER));
		Button play=new Button("PLAY");
		add(play);

		play.addActionListener(this);

		//      newGame();
	}

	void newGame()
	{
		pacMan=new MainFrame("0");
	}

	/////////////////////////////////////////////////
	// handles button event
	/////////////////////////////////////////////////
	public void actionPerformed(ActionEvent e)
	{
		if ( pacMan != null && ! pacMan.isFinalized() )
			// another is running
			return;
		newGame();
	}

}
